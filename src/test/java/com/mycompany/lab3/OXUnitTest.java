/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXUnitTest {

    public OXUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Verticel1_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Verticel2_output_true() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Verticel3_output_true() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Verticel1_output_false() {
        String[][] table = {{"O", "O", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWin_X_Verticel1_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Verticel2_output_false() {
        String[][] table = {{"-", "-", "-"}, {"X", "-", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWin_O_Horizontal1_output_true() {
        String[][] table = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Horizontal2_output_true() {
        String[][] table = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Horizontal3_output_true() {
        String[][] table = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Horizontal_output_false() {
        String[][] table = {{"-", "-", "O"}, {"-", "O", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWin_X_Horizontal1_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Horizontal_output_false() {
        String[][] table = {{"-", "-", "X"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void checkwinner_O_Diagonal_1_Output_true() {
        String[][] table = {{"O", "-", "-"},
                            {"-", "O", "-"},
                            {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);

    }
    @Test
    public void checkwinner_O_Diagonal_1_Output_false() {
        String[][] table = {{"O", "-", "-"},
                            {"-", "X", "-"},
                            {"-", "-", "O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);

    }
    
    @Test
    public void checkwinner_X_Diagonal_1_Output_true() {
        String[][] table = {{"-", "-", "X"},
                            {"-", "X", "-"},
                            {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);

    }
    
    @Test
    public void checkwinner_X_Diagonal_1_Output_false() {
        String[][] table = {{"-", "-", "O"},
                            {"-", "X", "-"},
                            {"X", "-", "-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);

    }
    
    @Test
    public void checkDraw_Output_true() {
        String[][] table = {{"O", "X", "O"},
        {"X", "X", "O"},
        {"X", "O", "X"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true, result);
         
      }
    
    @Test
    public void checkDraw_Output_false() {
        String[][] table = {{"O", "-", "O"},
                            {"X", "-", "-"},
                            {"-", "O", "X"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(false, result);
         
      }


}
