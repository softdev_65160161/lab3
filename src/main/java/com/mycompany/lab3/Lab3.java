/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {

    static String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    static String currentPlayer = "X";
    static int row, col;
    static Scanner kb = new Scanner(System.in);

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (isWinner(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    static boolean checkDraw(String[][] table) {
        if (isDraw(table)) {
            return true;
        }
        return false;
    }

    static boolean isWinner(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        if (checkDiagonal(table, currentPlayer)) {
            return true;
        }

        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer) {
                return true;
            }
        }
        return false;

    }

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer) {
                return true;
            }
        }
        return false;

    }

    private static boolean checkDiagonal(String[][] table, String currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[2][0] == currentPlayer && table[1][1] == currentPlayer && table[0][2] == currentPlayer) {
            return true;
        }
        return false;

    }

    private static boolean isDraw(String[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;

    }

    static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    static void inputRowCol() {
        while (true) {
            System.out.print("Please input row,col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == "-") {
                table[row - 1][col - 1] = currentPlayer;
                break;
            }

        }
    }

    static void changePlayer() {
        if (currentPlayer == "X") {
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }

    static void printWin() {
        System.out.println(currentPlayer + " is a winner!");
    }

    static void printDraw() {
        System.out.println("Tie No one win");
    }
    
    static void resetTable(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
    }
    
    static void resetTurn(){
        currentPlayer = "X";
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            resetTable();
            resetTurn();
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (checkWin(table, currentPlayer)) {
                    printTable();
                    printWin();
                    break;
                } else if (checkDraw(table)) {
                    printTable();
                    printDraw();
                    break;

                }

                changePlayer();
            }
            System.out.print("Please input continue or exit: ");
            String con = kb.next();
            if (con.equals("continue")){
                continue;
            }else if(con.equals("exit")){
                break;

            }
        }    

    }

}
